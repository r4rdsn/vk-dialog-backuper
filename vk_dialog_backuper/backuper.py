import re
import json
from datetime import datetime
import os.path
from zipfile import ZipFile
from vk_api import VkApi, VkTools, VkRequestsPool
from vk_api.execute import VkFunction
import urllib.request

from tqdm import tqdm

from .utils import ask_yes_or_no, logger


class VkDialogBackuper(VkApi):
    def __init__(self, *args, filename=None, directory=None, options='message', config=None, proxies=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.http.proxies = proxies

        if not self.token['access_token']:
            logger.info('Авторизация')
            self.auth()

        self.vk = self.get_api()
        self.tools = VkTools(self)

        logger.info('Получение информации о пользователе')
        with VkRequestsPool(self) as pool:
            user = pool.method('users.get')
            dialogs = pool.method('messages.getConversations')

        self.options = options.split(',')

        self.user_id = user.result[0]['id']
        dialog_count = dialogs.result['count']

        logger.info('Получение диалогов')
        self.dialogs = []
        for dialog in tqdm(self.get_all_iter('messages.getConversations'), total=dialog_count, leave=False):
            self.dialogs.append(dialog)

        self.config = {}
        if config:
            logger.info('Парсинг конфигурационного файла')
            with open(config, 'r') as file:
                for line in file:
                    match = re.match(r'(?:(user|chat|group|email)_)?(-?\d+)(?:\s+([a-z,]+))?', line)
                    if match:
                        peer_type, peer_id, config_options = match.groups()
                        peer_id = int(peer_id)
                        if peer_type:
                            if peer_type == 'chat':
                                peer_id += 2000000000
                            elif peer_type == 'group':
                                peer_id = -peer_id
                            elif peer_type == 'email':
                                peer_id = -peer_id - 2000000000
                        self.config[peer_id] = config_options.split(',') if config_options else []

        self.alias = {}

        if directory:
            self.output = 'dir'
            self.directory = directory
        else:
            self.output = 'zip'
            if not filename:
                filename = 'vk-dialog-backup-{}.zip'.format(datetime.now().strftime('%d-%m-%Y-%H-%M-%S'))
            elif os.path.splitext(filename)[1] != '.zip':
                filename += '.zip'
            logger.info('Открытие файла ' + filename)
            self.zipf = ZipFile(filename, 'w')

    def auth_handler(self):
        logger.info('Необходимо подтверждение авторизации')
        code = input('Введите код из личного сообщения от Администрации: ')
        remember = ask_yes_or_no('Запомнить этот аккаунт?')
        return code, remember

    def get_all_iter(self, method, max_count=200, **kwargs):
        return self.tools.get_all_iter(method, max_count, kwargs)

    def get_all_attachments(self, peer_id, media_type):
        result = []
        start_from = 0
        items_count = 0
        count = 200

        while True:
            response = vk_get_all_attachments(
                self, {'count': 200, 'media_type': media_type, 'peer_id': peer_id}, count, start_from
            )
            items = response["items"]
            result.extend(items)

            if not response['more']:
                break

            start_from = response['start_from']

        return result

    def run(self):
        try:
            self._run()
        finally:
            self._write('alias.json', self._dumps(self.alias))
            if self.output == 'zip':
                self.zipf.close()

    def _run(self):
        for dialog in self.dialogs:
            conversation = dialog['conversation']
            peer = conversation['peer']
            directory = '{}_{}'.format(peer['type'], peer['local_id'])
            if peer['type'] == 'chat':
                logger.info('Обработка беседы {} ({})'.format(peer['local_id'], conversation['chat_settings']['title']))
            else:
                logger.info('Обработка диалога с пользователем {}'.format(peer['local_id']))

            if self._check_option('leave', peer['id']):
                if peer['type'] == 'chat':
                    action = dialog['last_message']['action']
                    if not (action['type'] == 'chat_kick_user' and action['member_id'] == self.user_id):
                        self.vk.messages.remove_chat_user(chat_id=peer['local_id'], user_id=self.user_id)
                        logger.info('Выход из беседы {}'.format(peer['local_id']))

            if self._check_option('message', peer['id']):
                total_messages = self.vk.messages.get_history(peer_id=peer['id'])['count'] + 1
                logger.info('Сохранение {} сообщений'.format(total_messages))

                messages = []
                members = set()
                for message in tqdm(
                    self.get_all_iter('messages.getHistory', peer_id=peer['id']), total=total_messages, leave=False
                ):
                    messages.append(message)
                    members.add(str(message['from_id']))

                users = self.vk.users.get(user_ids=','.join(members))
                names = {u['id']: u['first_name'] + ' ' + u['last_name'] for u in users}

                filename = os.path.join(directory, 'messages.json')
                self._write(filename, self._dumps({'names': names, 'items': messages}))
                if peer['type'] == 'chat':
                    self.alias[filename] = conversation['chat_settings']['title']
                else:
                    self.alias[filename] = names[peer['id']]

            if self._check_option('photo', peer['id']):
                logger.info('Полученне фотографий')
                photos = self.get_all_attachments(peer['id'], 'photo')
                if photos:
                    logger.info('Сохранение {} фотографий'.format(len(photos)))
                    cache = set()
                    photobar = tqdm(photos, leave=False)
                    for attach in photobar:
                        photo = attach['attachment']['photo']
                        photo_name = '{}_{}'.format(photo['owner_id'], photo['id'])
                        if photo_name in cache:
                            continue
                        cache.add(photo_name)
                        for s in photo['sizes']:
                            if s['type'] == 'x':
                                photobar.set_description('Сохранение фотографии ' + photo_name)
                                content = urllib.request.urlopen(s['url']).read()
                                filename = os.path.join(directory, 'photos', photo_name + '.jpg')
                                self._write(filename, content, log=False)
                                break
                    cache = set()

            if self._check_option('delete', peer['id']):
                self.vk.messages.delete_dialog(peer_id=peer['id'])

    def _check_option(self, option, peer_id):
        if peer_id in self.config:
            return option in self.config[peer_id]
        else:
            return option in self.options

    def _dumps(self, obj):
        return json.dumps(obj, indent=4, ensure_ascii=False)

    def _write(self, filename, content, log=True):
        if log:
            logger.info('Сохранение файла ' + filename)

        if isinstance(content, bytes):
            data = content
            mode = 'w'
        elif isinstance(content, str):
            data = content.encode()
            mode = 'wb'
        else:
            raise TypeError

        if self.output == 'zip':
            with self.zipf.open(filename, 'w') as file:
                file.write(data)

        elif self.output == 'dir':
            with open(os.path.join(self.directory, filename), mode) as file:
                file.write(data)


vk_get_all_attachments = VkFunction(
    args=('values', 'count', 'start_from'),
    clean_args=('start_from'),
    code='''
    var params = %(values)s,
        calls = 0,
        items = [],
        count = %(count)s,
        start_from = %(start_from)s;

    while(calls < 25) {
        calls = calls + 1;

        params.start_from = start_from;
        var response = API.messages.getHistoryAttachments(params);

        if (response.items.length > 0) {
            items = items + response.items;
            start_from = response.next_from;
        } else {
            calls = 99;
        };
    };

    return {
        items: items,
        start_from: start_from,
        more: calls != 99
    };
''')
