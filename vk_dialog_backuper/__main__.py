'''\
usage: vk-dialog-backuper [options]

options:
    -h --help                 вывести это сообщение и выйти
    -v --version              вывести версию программы и выйти
    -t --token TOKEN          авторизоваться через токен
    -l --login LOGIN          авторизоваться с использованием указанного логина
    -p --password PASSWORD    авторизоваться с использованием указанного пароля
    -f --file FILE            сохранить бэкап в архив с указанным названием
    -d --directory DIRECTORY  сохранить бэкап в директорию с указанным названием
    -g --global OPTIONS       использовать указанные настройки для всех диалогов [default: message]
    -c --config FILE          использовать указанный конфиг
    --proxy URL               использовать указанный HTTP/HTTPS/SOCKS5 прокси
                              для использования SOCKS5 прокси, укажите протокол в адресе
                              например, socks5://127.0.0.1:1080/
    --api-version VERSION     использовать указанную версию VK API [default: 5.80]
'''

import sys
from getpass import getpass
from urllib.parse import urlparse

from vk_dialog_backuper import __version__
from .backuper import VkDialogBackuper
from .utils import docopt


def start_backuper(args):
    kwargs = {
        'filename': args['--file'],
        'directory': args['--directory'],
        'options': args['--global'],
        'config': args['--config'],
        'api_version': args['--api-version']
    }

    if args['--login'] and args['--password']:
        kwargs['login'] = args['--login']
        kwargs['password'] = args['--password']
    elif args['--token']:
        kwargs['token'] = args['--token']
    else:
        kwargs['login'] = input('Введите логин: ')
        kwargs['password'] = getpass('Введите пароль: ')

    if args['--proxy']:
        parsed = urlparse(args['--proxy'])
        if parsed.scheme == 'socks5':
            kwargs['proxies'] = {'http': args['--proxy'], 'https': args['--proxy']}
        else:
            kwargs['proxies'] = {'http': 'http://' + parsed.netloc, 'https': 'https://' + parsed.netloc}

    backuper = VkDialogBackuper(**kwargs)
    backuper.run()


def main():
    args = docopt(__doc__, version=__version__)

    try:
        start_backuper(args)
    except KeyboardInterrupt:
        print()
        sys.exit()


if __name__ == '__main__':
    main()
